import { createContext } from "react";

const PartieContext = createContext({});

export default PartieContext;