import './App.css';
import React, {useEffect, useState} from "react";

import UserContext from "./contexts/UserContext";
import userService from "./services/UserService";

import PartieContext from "./contexts/PartieContext";
import partieService from "./services/PartieService";

import ChalengerContext from "./contexts/ChalengerContext";
import chalengerService from "./services/ChalengerService";

import{
    BrowserRouter as Router,
    Route,
    Redirect,
    Switch
} from 'react-router-dom';

import Accueil from './components/accueil/Accueil';
import Connexion from './components/connexion/Connexion';
import Deconnexion from './components/deconnexion/Deconnexion'
import Creer from './components/creer/Creer';
import CreerRejoindre from './components/creerRejoindre/CreerRejoindre';
import MonCompte from './components/monCompte/MonCompte';
import Rejoindre from './components/rejoindre/Rejoindre';
import Start from './components/start/Start';
import Gagnant from './components/gagnant/Gagnant';
import NavBar from './components/navBar/NavBar';
import Api from './components/api/Api';
import Mediapipe from './components/game/Mediapipe';
import Register from './components/register/Register';
// import { create } from 'istanbul-reports';

const App = () => {
    const [user, setUser] = useState(null);
    const [partie, setPartie] = useState(null);
    const [chalenger, setChalenger] = useState(null);
    // const userContext = useContext(UserContext);

    useEffect(() => {
      return null;
    }, [user, partie])
    
    return(

    <div className="App">

         <UserContext.Provider value={{
            user: user,
            logOut: () => setUser(null),
            register: (login, password, onSuccess, onFail, onDone)=>
              userService.createUser(login, password)
              .then((newUser) => {
                setUser(newUser);
                onSuccess(newUser);
              })
              .catch((error) => onFail(error))
              .finally(onDone),
            logUser: (login, pass, onSuccess, onFail, onDone) => {
              userService.login(login, pass)
              .then((newUser) => {
                setUser(newUser);
                onSuccess(newUser);
              })
              .catch((error) => onFail(error))
              .finally(onDone);
            },
            getUser: (idUser, onSuccess, onFail, onDone) => {
              userService.getUser(idUser)
              .then((newUser) => {
                setUser(newUser);
                onSuccess(newUser);
              })
              .catch((error) => onFail(error))
              .finally(onDone);
            },
          }}>
         <PartieContext.Provider value={{
             partie: partie,
             unsetPartie: () => setPartie(null),
             getPartie: (idPartie, onSuccess, onFail, onDone) => {
                 partieService.getPartie(idPartie)
                     .then((newPartie) => {
                         setPartie(newPartie);
                         onSuccess(newPartie);
                     })
                     .catch((error) => onFail(error))
                     .finally(onDone);
             },
             createPartie: (idUser, onSuccess, onFail, onDone) => {
             partieService.createPartie(idUser)
                 .then((newPartie) => {
                     setPartie(newPartie);
                     onSuccess(newPartie);
                 })
                 .catch((error) => onFail(error))
                 .finally(onDone);
             },
             joinPartie: (idPartie, idUser2, onSuccess, onFail, onDone) => {
             partieService.joinPartie(idPartie, idUser2)
                 .then((newPartie) => {
                     setPartie(newPartie);
                     onSuccess(newPartie);
                 })
                 .catch((error) => onFail(error))
                 .finally(onDone);
             },
             updateSigne: (idPartie, signe, idUser1, onSuccess, onFail, onDone) => {
              partieService.updateSigne(idPartie, signe, idUser1)
                  .then((newPartie) => {
                      setPartie(newPartie);
                      onSuccess(newPartie);
                  })
                  .catch((error) => onFail(error))
                  .finally(onDone);
              }
         }}>
       <ChalengerContext.Provider value={{
           chalenger: chalenger,
           getChalenger: (idChalenger, onSuccess, onFail, onDone) => {
               chalengerService.getChalenger(idChalenger)
                   .then((newChalenger) => {
                       setChalenger(newChalenger);
                       onSuccess(newChalenger);
                   })
                   .catch((error) => onFail(error))
                   .finally(onDone);
           }
       }}>
       <Router>
           <NavBar/>
            <main>
                <Switch>
                    <Route path ="/" exact><Accueil/></Route>
                    <Route path ="/register" exact><Register/></Route> 
                    <Route path ="/connexion" exact><Connexion/></Route> 
                    <Route path ="/deconnexion" exact><Deconnexion/></Route> 
                    <Route path ="/monCompte" exact><MonCompte/></Route>
                    <Route path ="/creer" exact><Creer/></Route>
                    <Route path ="/creerRejoindre" exact><CreerRejoindre/></Route>
                    <Route path ="/rejoindre" exact><Rejoindre/></Route>
                    <Route path ="/game" exact><Mediapipe/></Route>
                    <Route path ="/api" exact><Api/></Route>
                    <Route path ="/start" exact><Start/></Route>
                    <Route path ="/gagnant" exact><Gagnant/></Route>
                    <Route path ="/" exact><Api/></Route>
                    <Redirect to ="/" />
                </Switch>
            </main>
       </Router>
       </ChalengerContext.Provider>
       </PartieContext.Provider>
       </UserContext.Provider>

       </div>
    );
}
  
export default App;