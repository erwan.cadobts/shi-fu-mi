import React, {useContext} from 'react';
import UserContext from "../../contexts/UserContext.js";
import { Redirect } from "react-router-dom";

const Deconnexion = () => {
    const userContext = useContext(UserContext);
    userContext.logOut();
    return <Redirect to='/accueil' />
}
  
export default Deconnexion;