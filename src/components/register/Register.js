import React, {useContext, useState} from 'react';
import {useHistory} from 'react-router-dom';
import UserContext from "../../contexts/UserContext.js";
import Loading from "../loading/Loading";

function Register() {
  const [mail, setMail] = useState("");
  const [password, setPassword] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const history = useHistory();
  const userContext = useContext(UserContext);

  return (
    <div className="login">
        <form onSubmit={(e) => {
            e.preventDefault();
            setIsLoading(true);
            userContext.register(mail, password, (u) => {alert("Bienvenue" + u.login); history.push("/");}, alert, () => setIsLoading(false));
        }}>
            <div>
                <label for="login">Mail/Login : </label>
                <input name="login" placeholder="mail/login" value={mail} onChange={(e) => setMail(e.target.value)}/>
            </div>
            <div>
                <label for="password">Password : </label>
                <input name="password" placeholder="pass" type="password" value={password} onChange={(e) => setPassword(e.target.value)}/>
            </div>
            { isLoading ? <Loading/> : <button type="submit">Valider</button> }
        </form>
    </div>
  );
}

export default Register;

// const Connexion = () => {
//     return(

//         <div className="container">
//             <form>
//                 <label>
//                     Nom :
//                     <input type="text" name="name" />
//                 </label>
//                 <label>
//                     Password :
//                     <input type="password" name="name" />
//                 </label>
//                 <input type="submit" value="Envoyer" />
//             </form>
//         </div>

//     );
// }

