import React, {useContext, useEffect} from 'react';
import UserContext from "../../contexts/UserContext.js";
import './NavBar.css';
import { NavLink } from 'react-router-dom';
import $ from 'jquery';

const Navbar = () => {
  const userContext = useContext(UserContext);

  function animation(){
    $("#navbarSupportedContent").on("click","li",function(e){
      $('#navbarSupportedContent ul li').removeClass("active");
      $(this).addClass('active');
      var activeWidthNewAnimHeight = $(this).innerHeight();
      var activeWidthNewAnimWidth = $(this).innerWidth();
      var itemPosNewAnimTop = $(this).position();
      var itemPosNewAnimLeft = $(this).position();
      $(".hori-selector").css({
        "top":itemPosNewAnimTop.top + "px", 
        "left":itemPosNewAnimLeft.left + "px",
        "height": activeWidthNewAnimHeight + "px",
        "width": activeWidthNewAnimWidth + "px"
      });
    });
  }

  useEffect(() => {
    
    animation();
    $(window).on('resize', function(){
      setTimeout(function(){ animation(); }, 500);
    });
    
  }, []);

  if(userContext.user != null) {
    return (

      <nav className="navbar navbar-expand-lg navbar-mainbg">
  
  
      <NavLink className="navbar-brand navbar-logo" to="/" exact>
       S h i - F u - M i
      </NavLink>
    
      <button 
        className="navbar-toggler"
        type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <i className="fas fa-bars text-white"></i>
      </button>
  
      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav ml-auto">
            <div className="hori-selector">
              <div className="left"></div>
              <div className="right"></div>
            </div>
            <li className="nav-item">
              <NavLink className="nav-link" to="/accueil" exact>
                <i 
                className="far fa-address-book">
                </i>Accueil
              </NavLink> 
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/creerRejoindre" exact>
                <i 
                className="far fa-clone">
                </i>Jouer
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/game" exact>
                <i 
                className="far fa-chart-bar">
                </i>Mediapipe
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/monCompte" exact>
                <i 
                className="far fa-chart-bar">
                </i>Mon Compte
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/deconnexion" exact>
                <i className="far fa-copy">
                </i>Se déconnecter
              </NavLink>
            </li>
        </ul>
      </div>
  </nav>
    )
  }
  else{
    return (

      <nav className="navbar navbar-expand-lg navbar-mainbg">
  
  
      <NavLink className="navbar-brand navbar-logo" to="/" exact>
       S h i - F u - M i
      </NavLink>
    
      <button 
        className="navbar-toggler"
        type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <i className="fas fa-bars text-white"></i>
      </button>
  
      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav ml-auto">
            <div className="hori-selector">
              <div className="left"></div>
              <div className="right"></div>
            </div>
            <li className="nav-item">
              <NavLink className="nav-link" to="/accueil" exact>
                <i 
                className="far fa-address-book">
                </i>Accueil
              </NavLink> 
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/creerRejoindre" exact>
                <i 
                className="far fa-clone">
                </i>Jouer
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/game" exact>
                <i 
                className="far fa-chart-bar">
                </i>Mediapipe
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/monCompte" exact>
                <i 
                className="far fa-chart-bar">
                </i>Mon Compte
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/connexion" exact>
                <i className="far fa-copy">
                </i>Se connecter
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/register" exact>
                <i className="far fa-copy">
                </i>S'enregistrer
              </NavLink>
            </li>
        </ul>
      </div>
  </nav>
    )
  }


 
}
export default Navbar;