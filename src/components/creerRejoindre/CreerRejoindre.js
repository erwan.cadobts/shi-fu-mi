import React, {useContext, useState} from "react";
import PartieContext from "../../contexts/PartieContext.js";
import UserContext from "../../contexts/UserContext.js";
import {useHistory} from 'react-router-dom';
import { NavLink } from 'react-router-dom';
import './creerRejoindre.css';



const CreerRejoindre = () => {
    const [isLoading, setIsLoading] = useState(false);
    const history = useHistory();
    const partieContext = useContext(PartieContext);
    const userContext = useContext(UserContext);

    const createPartie = () => {
        if(userContext.user != null) {
            let idUser = userContext.user._id;
            partieContext.createPartie(idUser, (p) => {alert("code de partie : " + p._id); history.push("/creer");}, alert, () => setIsLoading(false));
        }
    };

    if(userContext.user != null) {
        return(
            <div className="container">
                <h1>Commencer à jouer</h1>
                <br/><br/><br/><br/><br/>


                <div class="row">

                    <div class="col-md-1"></div>
                    <div class="col-md-4">
                        <div class="row">
                            <NavLink id="btnRejoindre" className="btn btn-outline-warning" to="/rejoindre" exact>
                                <i className="far fa-chart-bar">
                                </i>Rejoindre
                            </NavLink>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-4">
                        <div class="row">
                            <button id="btnCreer" className="btn btn-outline-warning" onClick={createPartie}>
                                <i className="far fa-chart-bar">
                                </i>Créer
                            </button>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>

        );
    }
    else{
        return(
            <div className="App">
                <h1>Vous devez vous identifier pour crée une partie</h1>
            </div>
        );
    }
}
  
export default CreerRejoindre;