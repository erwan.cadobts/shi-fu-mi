import Webcam from "react-webcam";
import React, {useContext, useState, useRef, useEffect} from "react";
import UserContext from "../../contexts/UserContext.js";
import PartieContext from "../../contexts/PartieContext.js";
import {useHistory} from 'react-router-dom';
import ChalengerContext from "../../contexts/ChalengerContext.js";

import {HAND_CONNECTIONS, Hands} from '@mediapipe/hands';

import * as cam from "@mediapipe/camera_utils";

import ciseaux from "../../assets/images/ciseaux.jpg";
import scissors from "../../assets/images/scissors.jpg";
import rock from "../../assets/images/rock.png";
import paper from "../../assets/images/paper.jpg";
import logo from "../../assets/images/logo.png";

function Start () {

    const history = useHistory();

    const [user1] = useState("");
    const [user2] = useState("");
    const partieContext = useContext(PartieContext);
    const userContext = useContext(UserContext);
    const chalengerContext = useContext(ChalengerContext);

    const webcamRef = useRef(null);
    const canvasRef = useRef(null);
    const connect = window.drawConnectors;
    let camera = null;

    const [signe2, setSigne2] = useState(logo);
    const [signe, setSigne] = useState("Nothing");
 
    function updateSigne(signe) {
        let codePartie = partieContext.partie._id;
        if(userContext.user != null) {
            let userNumber = 0;

            if(partieContext.partie.idUser1 === userContext.user._id) {
                userNumber = 1;
            }
            else{
                userNumber = 2;
            }
            partieContext.updateSigne(codePartie, signe, userNumber, (p) => { alert("idUser1 : " + p.idUser1); history.push("/gagnant");}, alert);
        }
    }

    function wait(ms){
        var start = new Date().getTime();
        var end = start;
        while(end < start + ms) {
        end = new Date().getTime();
    }

    }
    
    const delay = (n) =>{
        return new Promise(function(resolve){
            setTimeout(resolve,n*1000);
        });
    }
    
    const myFunction = (landmarks) => {
        let dataCSV2 = [];
        let data = [];
        let dist_eucli = null;
        let res_hand_side, res_posture = null;
        let better_dist = 2000;
    
        wait(20);
    
    
        for (let a of landmarks){
            data.push(a.x);
            data.push(a.y);
            data.push(a.z);
        }
        // data.push("Right");
        // data.push("scissors");
        // console.log(data);
    
        let json = require('./dataCSV.json');
    
        let keys = Object.keys(json);
        keys.forEach(function(key){
            dataCSV2.push(json[key]);
        });
    
        
    
        for (const row of dataCSV2) {
    
        
    
            let posture = row[row.length - 1];
            let hand_side = row[row.length - 2];
    
        
            row.pop();
            row.pop();
    
            
            let v1 = 0;
            let v2 = 0;
            let res = "";
            // for (let i=0; i < data.length; i++ ) {
            
    
            //     v1 += data[i];
            //     v2 += row[i];
            //     res += "(row["+i+"]-data["+i+"])*(row["+i+"]-data["+i+"])+";
            
                
            // }
        
            //  console.log(res);
            
            // dist_eucli = Math.sqrt(Math.pow((v1-v2),2));
            // dist_eucli = Math.sqrt((data[0]-row[0])*(data[0]-row[0])+(data[1]-row[1])*(data[1]-row[1])+(data[2]-row[2])*(data[2]-row[2])+(data[3]-row[3])*(data[3]-row[3])+(data[4]-row[4])*(data[4]-row[4])+(data[5]-row[5])*(data[5]-row[5])+(data[6]-row[6])*(data[6]-row[6])+(data[7]-row[7])*(data[7]-row[7])+(data[8]-row[8])*(data[8]-row[8])+(data[9]-row[9])*(data[9]-row[9])+(data[10]-row[10])*(data[10]-row[10])+(data[11]-row[11])*(data[11]-row[11])+(data[12]-row[12])*(data[12]-row[12])+(data[13]-row[13])*(data[13]-row[13])+(data[14]-row[14])*(data[14]-row[14])+(data[15]-row[15])*(data[15]-row[15])+(data[16]-row[16])*(data[16]-row[16])+(data[17]-row[17])*(data[17]-row[17])+(data[18]-row[18])*(data[18]-row[18])+(data[19]-row[19])*(data[19]-row[19])+(data[20]-row[20])*(data[20]-row[20])+(data[21]-row[21])*(data[21]-row[21])+(data[22]-row[22])*(data[22]-row[22])+(data[23]-row[23])*(data[23]-row[23])+(data[24]-row[24])*(data[24]-row[24])+(data[25]-row[25])*(data[25]-row[25])+(data[26]-row[26])*(data[26]-row[26])+(data[27]-row[27])*(data[27]-row[27])+(data[28]-row[28])*(data[28]-row[28])+(data[29]-row[29])*(data[29]-row[29])+(data[30]-row[30])*(data[30]-row[30])+(data[31]-row[31])*(data[31]-row[31])+(data[32]-row[32])*(data[32]-row[32])+(data[33]-row[33])*(data[33]-row[33])+(data[34]-row[34])*(data[34]-row[34])+(data[35]-row[35])*(data[35]-row[35])+(data[36]-row[36])*(data[36]-row[36])+(data[37]-row[37])*(data[37]-row[37])+(data[38]-row[38])*(data[38]-row[38])+(data[39]-row[39])*(data[39]-row[39])+(data[40]-row[40])*(data[40]-row[40])+(data[41]-row[41])*(data[41]-row[41])+(data[42]-row[42])*(data[42]-row[42])+(data[43]-row[43])*(data[43]-row[43])+(data[44]-row[44])*(data[44]-row[44])+(data[45]-row[45])*(data[45]-row[45])+(data[46]-row[46])*(data[46]-row[46])+(data[47]-row[47])*(data[47]-row[47])+(data[48]-row[48])*(data[48]-row[48])+(data[49]-row[49])*(data[49]-row[49])+(data[50]-row[50])*(data[50]-row[50])+(data[51]-row[51])*(data[51]-row[51])+(data[52]-row[52])*(data[52]-row[52])+(data[53]-row[53])*(data[53]-row[53])+(data[54]-row[54])*(data[54]-row[54])+(data[55]-row[55])*(data[55]-row[55])+(data[56]-row[56])*(data[56]-row[56])+(data[57]-row[57])*(data[57]-row[57])+(data[58]-row[58])*(data[58]-row[58])+(data[59]-row[59])*(data[59]-row[59])+(data[60]-row[60])*(data[60]-row[60])+(data[61]-row[61])*(data[61]-row[61])+(data[62]-row[62])*(data[62]-row[62]));
            
            //this
            dist_eucli = Math.sqrt((row[0]-data[0])*(row[0]-data[0])+(row[1]-data[1])*(row[1]-data[1])+(row[2]-data[2])*(row[2]-data[2])+(row[3]-data[3])*(row[3]-data[3])+(row[4]-data[4])*(row[4]-data[4])+(row[5]-data[5])*(row[5]-data[5])+(row[6]-data[6])*(row[6]-data[6])+(row[7]-data[7])*(row[7]-data[7])+(row[8]-data[8])*(row[8]-data[8])+(row[9]-data[9])*(row[9]-data[9])+(row[10]-data[10])*(row[10]-data[10])+(row[11]-data[11])*(row[11]-data[11])+(row[12]-data[12])*(row[12]-data[12])+(row[13]-data[13])*(row[13]-data[13])+(row[14]-data[14])*(row[14]-data[14])+(row[15]-data[15])*(row[15]-data[15])+(row[16]-data[16])*(row[16]-data[16])+(row[17]-data[17])*(row[17]-data[17])+(row[18]-data[18])*(row[18]-data[18])+(row[19]-data[19])*(row[19]-data[19])+(row[20]-data[20])*(row[20]-data[20])+(row[21]-data[21])*(row[21]-data[21])+(row[22]-data[22])*(row[22]-data[22])+(row[23]-data[23])*(row[23]-data[23])+(row[24]-data[24])*(row[24]-data[24])+(row[25]-data[25])*(row[25]-data[25])+(row[26]-data[26])*(row[26]-data[26])+(row[27]-data[27])*(row[27]-data[27])+(row[28]-data[28])*(row[28]-data[28])+(row[29]-data[29])*(row[29]-data[29])+(row[30]-data[30])*(row[30]-data[30])+(row[31]-data[31])*(row[31]-data[31])+(row[32]-data[32])*(row[32]-data[32])+(row[33]-data[33])*(row[33]-data[33])+(row[34]-data[34])*(row[34]-data[34])+(row[35]-data[35])*(row[35]-data[35])+(row[36]-data[36])*(row[36]-data[36])+(row[37]-data[37])*(row[37]-data[37])+(row[38]-data[38])*(row[38]-data[38])+(row[39]-data[39])*(row[39]-data[39])+(row[40]-data[40])*(row[40]-data[40])+(row[41]-data[41])*(row[41]-data[41])+(row[42]-data[42])*(row[42]-data[42])+(row[43]-data[43])*(row[43]-data[43])+(row[44]-data[44])*(row[44]-data[44])+(row[45]-data[45])*(row[45]-data[45])+(row[46]-data[46])*(row[46]-data[46])+(row[47]-data[47])*(row[47]-data[47])+(row[48]-data[48])*(row[48]-data[48])+(row[49]-data[49])*(row[49]-data[49])+(row[50]-data[50])*(row[50]-data[50])+(row[51]-data[51])*(row[51]-data[51])+(row[52]-data[52])*(row[52]-data[52])+(row[53]-data[53])*(row[53]-data[53])+(row[54]-data[54])*(row[54]-data[54])+(row[55]-data[55])*(row[55]-data[55])+(row[56]-data[56])*(row[56]-data[56])+(row[57]-data[57])*(row[57]-data[57])+(row[58]-data[58])*(row[58]-data[58])+(row[59]-data[59])*(row[59]-data[59])+(row[60]-data[60])*(row[60]-data[60])+(row[61]-data[61])*(row[61]-data[61])+(row[62]-data[62])*(row[62]-data[62]));
    
            // if (dist_eucli < better_dist) {
            //dist_eucli = Math.sqrt( sum( Math.pow( data - row, 2 ) ) );
    
    
            //dist_eucli = euclideandistance(data, row)
    
            // dist_eucli = eucli(data, row)
            //dist_eucli = euclideandistance(data, row)
    
    
            if (dist_eucli < better_dist) {
    
                better_dist = dist_eucli;
                res_hand_side = hand_side;
                res_posture = posture;
                // console.log(better_dist);
            }
    
            row.push(hand_side);
            row.push(posture);
        }
    
        
        setSigne(res_posture);

        if (res_posture == "rock"){
            setSigne2(rock);
        }
        if (res_posture == "scissors"){
            setSigne2(scissors);
        }
        if (res_posture == "paper"){
            setSigne2(paper);
        }

       
    
        //signe = res_posture;
        
        console.log(better_dist + " => " + res_hand_side + " with " + signe);
    };


    function onResults(results) {
        //const video = webcamRef.current.video;
        const videoWidth = webcamRef.current.video.videoWidth;
        const videoHeight = webcamRef.current.video.videoHeight;

        // Set canvas width
        canvasRef.current.width = videoWidth;
        canvasRef.current.height = videoHeight;

        const canvasElement = canvasRef.current;
        const canvasCtx = canvasElement.getContext("2d");

        canvasCtx.save();
        canvasCtx.clearRect(0, 0, canvasElement.width, canvasElement.height);
        canvasCtx.drawImage(
            results.image,
            0,
            0,
            canvasElement.width,
            canvasElement.height
        );
        if (results.multiHandLandmarks && results.multiHandedness) {
            for (let index = 0; index < results.multiHandLandmarks.length; index++) {
                const classification = results.multiHandedness[index];
                const isRightHand = classification.label === 'Right';
                const landmarks = results.multiHandLandmarks[index];
                // console.log(landmarks)
                connect(canvasCtx, landmarks, HAND_CONNECTIONS, {
                    color: isRightHand ? '#00FF00' : '#FF0000'
                });
                window.drawLandmarks(canvasCtx, landmarks, {
                    color: isRightHand ? '#00FF00' : '#FF0000',
                    fillColor: isRightHand ? '#FF0000' : '#00FF00',
                });
            
                myFunction(landmarks);
            }
        }
    
        
        canvasCtx.restore();
        
    }

    // }

    // setInterval(())
    useEffect(() => {
        const hands = new Hands({
            locateFile: (file) => {
                return `https://cdn.jsdelivr.net/npm/@mediapipe/hands/${file}`;
            }
        });

        hands.setOptions({
            maxNumHands: 1,
            minDetectionConfidence: 0.9,
            minTrackingConfidence: 0.9
        });
        
        hands.onResults(onResults);
        
        if (typeof webcamRef.current !== "undefined" && webcamRef.current !== null) {
            if (webcamRef.current.video !== null) {

                camera = new cam.Camera(webcamRef.current.video, {
                    onFrame: async () => {
                        await hands.send({image: webcamRef.current.video});
                    },
                    width: 640,
                    height: 480,
                });
                camera.start();
            }

        }
    }, [user1, user2]);
    return (
        <center>
            <div className="App">
                <h1>{userContext.user.login} VS {chalengerContext.chalenger.login}</h1>
            <div class="row">
                <div class="col-md-5">
                    <Webcam
                        ref={webcamRef} 
                        style={{
                            position: "absolute",
                            zindex: 9,
                            width: 440,
                            height: 360,
                        }}
                    />{" "}
                    <canvas
                        ref={canvasRef} 
                        style={{
                            position: "absolute",
                            zindex: 9,
                            width: 440,
                            height: 360,
                        }}
                    />
                </div>
                <div class="col-md-2">
                    
                </div>
                <div class="col-md-5">
                        <img src={signe2} alt="ciseaux" style={{ width: 440, height: 360 }}></img>
                </div>
            </div>

            <button onClick={ () => updateSigne(signe)  }>{signe}</button>

            </div>
        </center>
    );
}
  
export default Start;