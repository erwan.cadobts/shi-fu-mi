import React, {useContext} from "react";
import PartieContext from "../../contexts/PartieContext.js";
import UserContext from "../../contexts/UserContext.js";
import partieService from "../../services/PartieService";
import {useHistory} from 'react-router-dom';
import './Gagnant.css';
import { NavLink } from 'react-router-dom';
import ChalengerContext from "../../contexts/ChalengerContext";

const Gagnant = () => {

    const userContext = useContext(UserContext);
    const chalengerContext = useContext(ChalengerContext);
    const partieContext = useContext(PartieContext);
    const history = useHistory();

    let winner = "En attente de l'autre joueur ";


    if(partieContext.partie.signeUser1 != null && partieContext.partie.signeUser2 != null){
        winner = partieService.gameManagement(partieContext.partie.signeUser1, partieContext.partie.signeUser2);

        let player1 = "";
        let player2 = "";

        if(partieContext.partie.idUser1 === userContext.user._id) {
            player1 = userContext.user.login;
            player2 = chalengerContext.chalenger.login;
        }
        else{
            player1 = chalengerContext.chalenger.login;
            player2 = userContext.user.login;
        }

        if(winner === partieContext.partie.signeUser1) {
            winner = player1 + " avec : " + partieContext.partie.signeUser1;
        }
        else if (winner === partieContext.partie.signeUser2){
            winner = player2 + " avec : " + partieContext.partie.signeUser2;
        }
        else{
            winner = "Match Nul !";
        }

    }

    const refresh = () => {

        partieContext.getPartie(partieContext.partie._id, (p) => {
            if(p.signeUser2 === "" || p.signeUser1 === "") {
                alert("Le chalenger réfléchis encore à son signe.");
            }
            else{
                history.push("/gagnant");
            }
        }, alert);
    }

    return(

        <div className="App">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-11"></div>
                            <div class="col-md-1">

                                <button id="actualiser" class="btn btn-outline-secondary"  onClick={refresh} >
                                    Actualiser
                                </button>

                            </div>
                        </div>

                     {/* <div class="row"> 
                   
                         <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-4"><img id="image" src={logo} alt="camera"/></div>
                            <div class="col-md-2"></div>
                            <div class="col-md-4"><img id="image" src={logo} alt="camera"/></div>
                            <div class="col-md-1"></div>
                
                        </div> */}

                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-3"><img id="image2" src="https://media.istockphoto.com/vectors/rock-paper-scissors-vector-id513629863?k=6&m=513629863&s=612x612&w=0&h=sYOAGA50aMaHiDn2zkEZocoDP0vbl2kMl_Wn7vni2IU=" alt="Canvas Logo"/></div>
                            <div class="col-md-2"><img id="image3" src="https://st3.depositphotos.com/1800192/14757/v/600/depositphotos_147572465-stock-illustration-versus-sign-in-comic-style.jpg" alt="Canvas Logo"/></div>
                            <div class="col-md-3"><img id="image2" src="https://media.istockphoto.com/vectors/rock-paper-scissors-vector-id513629863?k=6&m=513629863&s=612x612&w=0&h=sYOAGA50aMaHiDn2zkEZocoDP0vbl2kMl_Wn7vni2IU=" alt="Canvas Logo"/></div>
                            <div class="col-md-2"></div>
                

                        <div class="row">
                        
                            <div class="col-md-3"></div>
                           
                            <div class="col-md-6">
                                <ul class="list-group">
                                    <li id="textGagnant" class="list-group-item">{winner}</li>
                                </ul>

                                <button>
                                    Rejouer
                                </button>

                            </div>
                            
                            <div class="col-md-3"></div>
                
                        </div>
                        {/* </div> */}
                       
                </div>

                </div>
            </div>
        </div>

    );
}
  
export default Gagnant;