import React, {useContext, useState} from "react";
import PartieContext from "../../contexts/PartieContext.js";
import {useHistory} from 'react-router-dom';
import UserContext from "../../contexts/UserContext.js";
import ChalengerContext from "../../contexts/ChalengerContext";


const Rejoindre = () => {
    const history = useHistory();
    const partieContext = useContext(PartieContext);
    const userContext = useContext(UserContext);
    const chalengerContext = useContext(ChalengerContext);

    const [codePartie, setCodePartie] = useState('60e1ec63bfa42e1abc71b5fe');
    const handleChange = (e) => setCodePartie(e.target.value);

    const joinPartie = () => {

        alert("Join partie : " + codePartie);

        if(userContext.user != null) {
            partieContext.joinPartie(codePartie, userContext.user._id, (p) => {

                alert("partie trouvé : " + p._id);

                switch(userContext.user._id){
                    case p.idUser1:
                        chalengerContext.getChalenger(p.idUser2, (c2) => {alert("Adversaire : " + c2.login); history.push("/start"); }, alert);
                        break;
                    case p.idUser2:
                        chalengerContext.getChalenger(p.idUser1, (c2) => {alert("Adversaire : " + c2.login); history.push("/start");  }, alert);
                        break;
                    default:
                        console.log("Aucun user ne corespond à " + userContext.user.login);
                        break;
                }
            }, alert);


        }
    };




    return(
        <div className="container" style={{paddingTop: "7%"}}>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <h1 className="text-center" >
                        Rejoindre une partie
                    </h1>
                        
                    <div class="row">
                        <input name="code" type="text" class="form-control" value={codePartie} onChange={handleChange} />
                    </div>
                    <br/>
                    <div class="row">
                        <button id="btnCreer" className="btn btn-outline-warning" onClick={joinPartie}>
                            <i className="far fa-chart-bar">
                            </i>Start
                        </button>
                    </div>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>
    );
}
  
export default Rejoindre;