import React, {useContext} from 'react';
import UserContext from "../../contexts/UserContext.js";

const Accueil = () => {
    const userContext = useContext(UserContext);

    if(userContext.user != null) {
        return(
            <div className="container">
                <h1 className="text-center"> Bienvenue {userContext.user.login }</h1>
                <h1 className="text-center">
                    Accueil
                </h1>
                <h1 className="text-center">Règle du Pierre – Feuille – Ciseaux</h1>
                <h3>Les différentes formes de main au Pierre – Feuille – Ciseaux :</h3>
                <p>La pierre : est représentée par un poing fermé.</p>
                <p>La feuille : est représentée par une main à plat, la paume en direction du sol.</p>
                <p>Les ciseaux : sont représentés par deux doigts formant un V.</p>
                <br/>
                <h3>Force de chaque forme au Pierre – Feuille – Ciseaux :</h3>
                <p>La pierre écrase les ciseaux et gagne.</p>
                <p>La feuille enveloppe la pierre et gagne.</p>
                <p>Les ciseaux découpent la feuille et gagnent.</p>
                <p>A partir de là chaque forme en bat une autre et perd contre une autre. Voici les forces en général à Shifumi.</p>
                <br/>
                <h3>Commentaire jouer au Chifoumi :</h3>
                <p>Le jeu se joue généralement en duel même s'il est possible de s'affronter à plusieurs. Pour commencer les joueurs jusqu'à trois en mettant la main dans le dos.</p>
                <p>Une fois à trois les joueurs révèlent leur principal (pierre, feuille ou ciseaux) en même temps. La plus forte des formes l'emporte et le joueur marque le point gagnant.</p>
                <p>Si les deux joueurs utilisent la même forme c'est un match nul. A savoir que le jeu se déroule généralement dans une manche gagnante. Mais pour plus de plaisir de jeu, il est bien de jouer au meilleur</p>
                <p>des trois manches (le premier à deux points remporte la partie) ou cinq manches (premier à trois points). A vous de décider !</p>
            </div>
        );
    }
    else{
        return(
            <div className="container">
                <h1 className="text-center">
                    Accueil
                </h1>
                <h1 className="text-center">Règle du Pierre – Feuille – Ciseaux</h1>
                <h3>Les différentes formes de main au Pierre – Feuille – Ciseaux :</h3>
                <p>La pierre : est représentée par un poing fermé.</p>
                <p>La feuille : est représentée par une main à plat, la paume en direction du sol.</p>
                <p>Les ciseaux : sont représentés par deux doigts formant un V.</p>
                <br/>
                <h3>Force de chaque forme au Pierre – Feuille – Ciseaux :</h3>
                <p>La pierre écrase les ciseaux et gagne.</p>
                <p>La feuille enveloppe la pierre et gagne.</p>
                <p>Les ciseaux découpent la feuille et gagnent.</p>
                <p>A partir de là chaque forme en bat une autre et perd contre une autre. Voici les forces en général à Shifumi.</p>
                <br/>
                <h3>Commentaire jouer au Chifoumi :</h3>
                <p>Le jeu se joue généralement en duel même s'il est possible de s'affronter à plusieurs. Pour commencer les joueurs jusqu'à trois en mettant la main dans le dos.</p>
                <p>Une fois à trois les joueurs révèlent leur principal (pierre, feuille ou ciseaux) en même temps. La plus forte des formes l'emporte et le joueur marque le point gagnant.</p>
                <p>Si les deux joueurs utilisent la même forme c'est un match nul. A savoir que le jeu se déroule généralement dans une manche gagnante. Mais pour plus de plaisir de jeu, il est bien de jouer au meilleur</p>
                <p>des trois manches (le premier à deux points remporte la partie) ou cinq manches (premier à trois points). A vous de décider !</p> 
            </div>
        );
    }
}

export default Accueil;