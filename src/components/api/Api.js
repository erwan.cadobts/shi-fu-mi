import React, { useState } from "react";
import Axios from 'axios';

function Api() {

    const [joke, setJoke] = useState(null);

    const [state, setState] = useState('');

    const handler = (event) => {
        // changing the state to the name of the key
        // which is pressed
        setState(event.key);
        if (event.key === "v") {
            console.log("VALIDER");
        }
    };

    return (
        <div>
            <h1> Test </h1>
            <p>Key pressed is: {state}</p>
            <button onKeyPress={(e) => handler(e)} >Valider</button>
        </div>
      );
}

export default Api;
