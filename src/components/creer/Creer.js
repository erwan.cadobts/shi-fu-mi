import Webcam from "react-webcam";
import React, {useContext, useRef, useEffect} from "react";
import {HAND_CONNECTIONS, Hands} from '@mediapipe/hands';
import UserContext from "../../contexts/UserContext.js";
import PartieContext from "../../contexts/PartieContext.js";
import {useHistory} from 'react-router-dom';
import * as cam from "@mediapipe/camera_utils";
import './Creer.css';
import ChalengerContext from "../../contexts/ChalengerContext";

function Creer() {

    const userContext = useContext(UserContext);
    const partieContext = useContext(PartieContext);
    const chalengerContext = useContext(ChalengerContext);
    const webcamRef = useRef(null);
    const history = useHistory();
    const canvasRef = useRef(null);
    const connect = window.drawConnectors;
    let camera = null;

    const checkChalengerJoin = () => {
        alert("on appelle la fonction de test");
            partieContext.getPartie(partieContext.partie._id, (p) => {
                if(p.idUser2 === "") {
                    alert("Le chalenger ne vous à pas encore rejoint");
                }
                else{
                    switch(userContext.user._id){
                        case p.idUser1:
                            chalengerContext.getChalenger(p.idUser2, (c2) => {alert("Adversaire : " + c2.login); history.push("/start"); }, alert);
                            break;
                        case p.idUser2:
                            chalengerContext.getChalenger(p.idUser1, (c2) => {alert("Adversaire : " + c2.login); history.push("/start");  }, alert);
                            break;
                        default:
                            console.log("Aucun user ne corespond à " + userContext.user.login);
                            break;
                    }
                }
            }, alert);
    };

    function onResults(results) {

        //const video = webcamRef.current.video;

        const videoWidth = webcamRef.current.video.videoWidth;
        const videoHeight = webcamRef.current.video.videoHeight;

        // Set canvas width
        canvasRef.current.width = videoWidth;
        canvasRef.current.height = videoHeight;

        const canvasElement = canvasRef.current;
        const canvasCtx = canvasElement.getContext("2d");

        canvasCtx.save();
        canvasCtx.clearRect(0, 0, canvasElement.width, canvasElement.height);
        canvasCtx.drawImage(
            results.image,
            0,
            0,
            canvasElement.width,
            canvasElement.height
        );
        if (results.multiHandLandmarks && results.multiHandedness) {
            for (let index = 0; index < results.multiHandLandmarks.length; index++) {
                const classification = results.multiHandedness[index];
                const isRightHand = classification.label === 'Right';
                const landmarks = results.multiHandLandmarks[index];
                connect(canvasCtx, landmarks, HAND_CONNECTIONS, {
                    color: isRightHand ? '#00FF00' : '#FF0000'
                });
                window.drawLandmarks(canvasCtx, landmarks, {
                    color: isRightHand ? '#00FF00' : '#FF0000',
                    fillColor: isRightHand ? '#FF0000' : '#00FF00',
                });
            }
        }
        canvasCtx.restore();
    }

    // }

    // setInterval(())
    useEffect(() => {
        const hands = new Hands({
            locateFile: (file) => {
                return `https://cdn.jsdelivr.net/npm/@mediapipe/hands/${file}`;
            }
        });

        hands.setOptions({
            maxNumHands: 1,
            minDetectionConfidence: 0.9,
            minTrackingConfidence: 0.9
        });

        hands.onResults(onResults);

        if (typeof webcamRef.current !== "undefined" && webcamRef.current !== null) {
            if (webcamRef.current.video !== null) {

                camera = new cam.Camera(webcamRef.current.video, {
                    onFrame: async () => {
                        await hands.send({image: webcamRef.current.video});
                    },
                    width: 640,
                    height: 480,
                });
                camera.start();
            }

        }
    }, []);

    if(userContext.user != null && partieContext.partie != null) {
        return (

            <div className="App">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-11"></div>
                            <div class="col-md-1">
                                <button type="button" id="actualiser" class="btn btn-outline-secondary">Actualiser</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                <ul class="list-group">
                                    <li class="list-group-item">Partager ce code à votre ami</li>
                                    <li class="list-group-item">{ partieContext.partie._id }</li>
                                </ul>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <Webcam
                                    ref={webcamRef}
                                    style={{
                                        position: "flex",
                                        marginLeft: "auto",
                                        marginRight: "auto",
                                        left: 0,
                                        right: 0,
                                        textAlign: "center",
                                        zindex: 9,
                                        height: 480,
                                    }}
                                />{" "}
                                <canvas
                                    ref={canvasRef}
                                    style={{
                                        position: "absolute",
                                        marginLeft: "auto",
                                        marginRight: "auto",
                                        left: 0,
                                        right: 0,
                                        textAlign: "center",
                                        zindex: 9,
                                        height: 480,
                                    }}
                                    className="output_canvas"
                                />
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                <div class="row">
                                    <button className="btn btn-outline-warning" onClick={checkChalengerJoin}>
                                        <i className="far fa-chart-bar"></i>Start
                                    </button>
                                </div>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
    else{
        return(
            <div className="App">
                <h1>Vous devez vous identifier pour créer une partie</h1>
            </div>
        );

    }

}
  
export default Creer;