import AbstractService from "./AbstractService";
import Axios from 'axios';


class PartieService extends AbstractService {


    async getPartie(idPartie) {

        console.log("idPartie : " + idPartie);

        let promise;
        let partie;

        // let url_test = "http://localhost:4000/api/parties/"
        let url_server = "http://alexperrot.fr/api/parties/"

        await Axios.get(url_server + idPartie).then(
            (response) => {
                console.log(response);
                partie = response.data;
            }
        );

        promise = this.getRequest(partie, true);
        console.log(promise);
        return promise;
    }

    async createPartie(idUser1) {
        let promise;
        let partie;
        //let url_test = "http://localhost:4000/api/parties/"
        let url_server = "http://alexperrot.fr/api/parties/"
        await Axios.post(url_server, {
            idUser1: idUser1,
            idUser2: ""
        }).then(
            (response) => {
                console.log(response);
                partie = response.data;
            }
        );

        promise = this.getRequest(partie, true);
        return promise;
    }

    async joinPartie(idPartie, idUser2) {
        let promise;
        let partie;
        //let url_test = "http://localhost:4000/api/parties/rejoindre/" + idPartie
        let url_server = "http://alexperrot.fr/api/parties/rejoindre/" + idPartie
        await Axios.put(url_server, {
            idUser2: idUser2
        }).then(
            (response) => {
                console.log(response);
                partie = response.data;
            }
        );

        console.log(partie);


        promise = this.getRequest(partie, true);
        console.log(promise);
        return promise;
    }


    async updateSigne(idPartie, signe, userNumber) {

        alert("On entre dans la fonction update signe.");
        let promise;
        let partie;

        if(userNumber === 1) {
            partie = this.updateSigneUser1(idPartie, signe);
            console.log(partie);
        }
        else{
            partie = this.updateSigneUser2(idPartie, signe);
            console.log(partie);
        }

        promise = this.getRequest(partie, true);
        console.log("résultat final : ");
        console.log(promise);
        return promise;
    }

    gameManagement(sign1, sign2) {
       //scissors paper rock
       let winnerSign = null;

        if(sign1 === "paper" && sign2 === "rock") {
            winnerSign = sign1;
        }

        switch(sign1) {
            case "rock":
                if(sign2 === "paper") {
                    winnerSign = sign2;
                }
                if(sign2 === "scissors") {
                    winnerSign = sign1;
                }
                break;
            case "paper":
                if(sign2 === "rock") {
                    winnerSign = sign1;
                }
                if(sign2 === "scissors") {
                    winnerSign = sign2;
                }
                break;
            case "scissors":
                if(sign2 === "rock") {
                    winnerSign = sign2;
                }
                if(sign2 === "paper") {
                    winnerSign = sign1;
                }
                break;
            default:
                alert("Error was detected !!!");
                break;
        }

        return winnerSign;
    }


    async updateSigneUser1(idPartie, signe) {

        let partie;

        let url_server = "http://alexperrot.fr/api/parties/updateSigne/" + idPartie;

        await Axios.put(url_server, {
            signeUser1: signe
        }).then(
            (response) => {
                console.log(response);
                partie = response.data;
            }
        );

        return partie;
    }

    async updateSigneUser2(idPartie, signe) {

        let partie;

        let url_server = "http://alexperrot.fr/api/parties/updateSigne/" + idPartie;

        await Axios.put(url_server, {
            signeUser2: signe
        }).then(
            (response) => {
                console.log(response);
                partie = response.data;
            }
        );

        return partie;
    }


}

const partieService = new PartieService();
export default partieService;